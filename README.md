# Motor driver with position feedback HAT

This repo contains a design for a motor driver with position feedback HAT that can be attached to the 40 GPIO pins of the Pi. It is intended to be used to track and control the instantaneous position of the M1N10FB11G Brushless DC Motor which has an incremental encoder attached to its shaft. The scenarios that the daughter board with a Pi are used in include solar tracking systems, automatic sliding gate and a hybrid engine fan. 

## Overview
* [Using this HAT](./PCB/GettingStarted.pdf)
* [Design References](./Design/): Contains Requirements, Specifications, Design Rule Checks, and SPICE simulations
* [Power Supply Submodule](./Power Supply Submodule/): Contains captioned screenshots of the simulation and KiCAD schematic
* [Status LEDs Submodule](./Status LEDs Submodule/): Contains captioned screenshots of the simulation, KiCAD schematic, and describes to a user how they should interpret the meaning of the LEDs
* [Amplifier Submodule](./Amplifier Submodule/): Contains captioned screenshots of the simulation, KiCAD schematic, and describes to a user how they should connect anything up to the Amplifier or to use it
* [PCB](./PCB/): contains reference manual, manufacturing notes, bill of materials and describes to a user how they should use the motor driver with position feedback HAT
* This repository is licensed under the ...[...LICENSE](LICENSE)

